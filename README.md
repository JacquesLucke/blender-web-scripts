This repository contains scripts that simplify working with Blender websites.

To use the `gitea-tampermonkey.js` script, you have to install the [Tampermonkey](https://www.tampermonkey.net/) browser extension. It has the following features:
* Adds a `Curl Apply` button for pull requests that copies a command of the form `curl url/of/diff | git apply`.
* Adds a `Paste Commit` button for repositories that opens a specific commit hash from the clipboard in that repository.
* Adds a `Weekly` button for pull requests and commits that copy a line that can be used in a weekly report.
