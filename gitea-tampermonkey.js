// ==UserScript==
// @name         Gitea Cleanup
// @version      0.1
// @description  Simplify the Gitea Interface
// @match        https://projects.blender.org/*
// @grant        GM.setClipboard
// ==/UserScript==

(function () {
  "use strict";

  function addPullRequestButton(label, onclick) {
    const parent = document.querySelector(
      "body > div > div.page-content.repository.view.issue.pull > div.ui.container > div.issue-title-header > div.issue-title-meta"
    );
    if (!parent) {
      console.log("Didn't find a place to add pull request button.");
      return;
    }
    const button = document.createElement("button");
    button.textContent = label;
    button.addEventListener("click", onclick);
    parent.appendChild(button);
  }

  function addRepositoryButton(label, onclick) {
    const parent = document.querySelector(
      "body > div > div.page-content.repository.file.list > div.ui.container > div.repo-button-row"
    );
    if (!parent) {
      console.log("Didn't find a place to add repository button.");
      return;
    }
    const button = document.createElement("button");
    button.textContent = label;
    button.addEventListener("click", onclick);
    parent.appendChild(button);
  }

  function addCommitButton(label, onclick) {
    const parent = document.querySelector(
      "body > div > div.page-content.repository.diff > div.ui.container.fluid.padded > div:nth-child(3) > div.diff-detail-box.diff-box"
    );
    if (!parent) {
      console.log("Didn't find a place to add commit button.");
      return;
    }
    const button = document.createElement("button");
    button.textContent = label;
    button.addEventListener("click", onclick);
    parent.appendChild(button);
  }

  addPullRequestButton("Curl Apply", function () {
    const currentUrl = window.location.href;
    const pullRequestUrl = currentUrl.match(/(.*pulls\/[0-9]+)/)[0];
    const diffUrl = pullRequestUrl + ".diff";
    const command = `curl ${diffUrl} | git apply`;
    GM.setClipboard(command);
  });

  addPullRequestButton("Weekly", function () {
    const currentUrl = window.location.href;
    const pullRequestID = currentUrl.match(/.*pulls\/([0-9]+)/)[1];
    const raw_title = document.getElementById("issue-title").textContent;
    const title = raw_title.substring(0, raw_title.lastIndexOf("#") - 1);
    const line = `* ${title} ({{PullRequest|${pullRequestID}}})`;
    GM.setClipboard(line);
  });

  addRepositoryButton("Paste Commit", function () {
    navigator.clipboard.readText().then(function (commitHash) {
      const currentUrl = window.location.href;
      const repositoryUrl = currentUrl.match(
        /(.*\.org\/[a-zA-Z\-_]+\/[a-zA-Z\-_]+)/
      )[0];
      const commitUrl = `${repositoryUrl}/commit/${commitHash}`;
      window.location.href = commitUrl;
    });
  });

  addCommitButton("Weekly", function () {
    const currentUrl = window.location.href;
    const commitId = currentUrl.match(/.*commit\/([a-f0-9]+)/)[1];
    const title = document
      .getElementsByClassName("commit-summary")[0]
      .getAttribute("title");
    const line = `* ${title} ({{GitCommit|${commitId}}})`;
    GM.setClipboard(line);
  });
})();
